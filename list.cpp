#include <iostream>
#include "list.h"
using namespace std;

List::~List() {
	for(Node *p; !isEmpty(); ) {
		p=head->next;
		delete head;
		head = p;
	}
}

void List::pushToHead(char el)
{
	head = new Node(el, head);
	if(tail==0)
	{
		tail = head;
	}
}
void List::pushToTail(char el)
{
	if(head == 0){
		pushToHead(el);
	}else{
	Node *tmp = new Node(el);
	tail->next = tmp;
	tail = tmp;
//	tail->next = 0;
	}
}
char List::popHead()
{
	char el = head->data;
	Node *tmp = head;
	if(head == tail)
	{
		head = tail = 0;
	}
	else
	{
		head = head->next;
	}
	delete tmp;
	return el;
}
char List::popTail()
{
	Node *tmp = head;//assign the tmp node point to head pointer
	char el;
	if(head == tail){ //in case of there's only one member in the list
		popHead();
	}else{
		while(tmp->next->next != 0){//finding tail node
			tmp = tmp->next;
		}
		tail = tmp;
		tmp = tmp->next;
		el = tmp -> data;
		delete tmp;
		tail->next = 0;
		return el;
	}
}
bool List::search(char el)
{
	Node *tmp = head;
	while(tmp->data != 0){
		if(tmp->data == el){
			return true;
		}else if(tmp->next == 0 && tmp->data != el){
			return false;
		}
		tmp = tmp->next;
	}
}
void List::reverse()
{
	int Listmember = 0;		//i need something to indicate how many of the list did i swap
	Node *tmp = head;
	while(tmp-> next != null){
		tmp = tmp->next;
		Listmember++;
	}

	Node *tmp1 = head;
	Node *tmp2 = tail;
	Node *tmp3 = tmp1->next;
	Node *tmp4 = tmp3->next;
	while(tmp4->next!=tail){
		tmp4 = tmp4->next;
	}
	while(tmp3->next!=tmp4){
		tmp3 = tmp3->next;
	}
	tmp2->next = tmp4;
	/*tmp4->next = null;
	tmp3->next = null;
	tmp4->next = tmp3;
	tmp4 = tmp1
	while(tmp4->next != tmp3){
		tmp4 = tmp4->next;
	}
	tmp3->next = null;
	tmp4->next = null;
	tmp3->next = tmp4;
	tmp3 = tmp1;
	while(tmp3->next != tmp4){
		tmp3 = tmp3->next;
	}
	tmp4->next = null;
	tmp3->next = null;
	tmp4->next = tmp3;
	tmp4 = tmp1
	while(tmp4->next != tmp3){
		tmp4 = tmp4->next;
	}*/
//due to postion of tmp3 and tmp4 i have to divide into 2 case which is wheter List is odd or even
	if(Listmember%2=0){          //even member case
		while(Listmember!=1){
			tmp4->next = null;
			tmp3->next = null;
			tmp4->next = tmp3;
			Listmember--;
			if(Listmember==1){
				break;
			}
			tmp4 = tmp1;
			while(tmp4->next != tmp3){
				tmp4 = tmp4->next;
			}
			tmp3->next = null;
			tmp4->next = null;
			tmp3->next = tmp4;
			Listmember--;
			tmp3 = tmp1;
			while(tmp3->next != tmp4){
				tmp3 = tmp3->next;
			}
		}
		tmp3->next = tmp1;
		head = tmp2;
		tail = tmp1;
	}

	if(Listmember%2!=0){          //odd member case
		while(Listmember!=1){
			tmp4->next = null;
			tmp3->next = null;
			tmp4->next = tmp3;
			Listmember--;
			if(Listmember==1){
				break;
			}
			tmp4 = tmp1;
			while(tmp4->next != tmp3){
				tmp4 = tmp4->next;
			}
			tmp3->next = null;
			tmp4->next = null;
			tmp3->next = tmp4;
			Listmember--;
			tmp3 = tmp1;
			while(tmp3->next != tmp4){
				tmp3 = tmp3->next;
			}
		}
		tmp4->next = tmp1;
		head = tmp2;
		tail = tmp1;
	}
}
void List::print()
{
	if(head  == tail)
	{
		cout << head->data;
	}
	else
	{
		Node *tmp = head;
		while(tmp!=tail)
		{
			cout << tmp->data;
			tmp = tmp->next;
		}
		cout << tmp->data;
	}
}