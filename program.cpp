#include <iostream>
#include "list.h"
using namespace std;

void main()
{
	//Sample Code
	List mylist;
	mylist.pushToHead('k');
	mylist.pushToHead('e');
	mylist.pushToHead('n');

	//mylist.pushToTail('a');

	mylist.print();

	if(mylist.search('a')){
		cout << endl << "True";
	}else{
		cout << endl << "False";
	}
	//cout <<endl << mylist.popHead() << endl;
	//cout <<endl << mylist.popTail() << endl;

	//TO DO! Write a program that tests your list library - the code should take characters, push them onto a list, 
	//- then reverse the list to see if it is a palindrome!
	
}